﻿using System;
using WebServiceTester.CurrencyConvertorService;

namespace WebServiceTester
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new ServiceReference1.CurrencyConvertorSoapClient();
            var result = service.ConversionRate(ServiceReference1.Currency.USD, ServiceReference1.Currency.PEN);
            Console.WriteLine("conversion rate: {0}",result);
            Console.ReadLine();
        }
    }
}
