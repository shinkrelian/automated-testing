﻿using NUnit.Framework;
using TechTalk.SpecFlow;

namespace Gherkin
{
    [Binding]
    public class MyFirstFeatureSteps
    {
        private int _operador1;
        private int _operador2;
        private int _resultado;

        [Given(@"I have entered operador1 as (.*) into the calculator")]
        public void DadoIHaveEnteredOperador1IntoTheCalculator(int p0)
        {
            _operador1 = p0;
        }

        [Given(@"I have entered operador2 as (.*) into the calculator")]
        public void DadoIHaveEnteredOperador2IntoTheCalculator(int p0)
        {
            _operador2 = p0;
        }

        [When(@"I press add")]
        public void CuandoIPressAdd()
        {
            var calculator = new Calculator.Calculator();
            _resultado = calculator.Suma(_operador1, _operador2);
        }

        [Then(@"the result should be (.*) on the screen")]
        public void EntoncesTheResultShouldBeOnTheScreen(string p0)
        {
            Assert.AreEqual(_operador1 + _operador2, _resultado);
        }
    }
}
