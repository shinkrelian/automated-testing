﻿#language: es
@Feature1
Característica: MyFirstFeature
	In order to avoid silly mistakes
	As a math idiot
	I want to be told the sum of two numbers
@Rapido
Esquema del escenario: One
	Dado I have entered operador1 as <numero1> into the calculator
	Y I have entered operador2 as <numero2> into the calculator
	Cuando I press add
	Entonces the result should be <tesultado> on the screen

	Ejemplos:
	| numero1 | numero2 | resultado |
	| 1       | 2       | 3         |
	| 2       | 5       | 7         |
	| 3       | 7       | 10        |
	| 6       | 2       | 7         |
