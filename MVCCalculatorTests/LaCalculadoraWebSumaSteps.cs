﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace MVCCalculatorTests
{
    [Binding]
    public class LaCalculadoraWebSumaSteps
    {
        private static IWebDriver _driver;
        private static string _baseUrl;

        [BeforeScenario]
        public static void SetupTest()
        {
            _driver = new FirefoxDriver();
            _baseUrl = "http://localhost/MvcCalculator/Calculator";
            _driver.Navigate().GoToUrl(_baseUrl);
        }

        [AfterScenario]
        public static void TeardownTest()
        {
            try
            {
                _driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
        }

        [Given(@"que el primer numero es (.*)")]
        public void DadoQueElPrimerNumeroEs(int operadorA)
        {
            _driver.FindElement(By.Id("Operador1")).SendKeys(operadorA.ToString());
        }

        [Given(@"que el segundo numero es (.*)")]
        public void DadoQueElSegundoNumeroEs(int operadorB)
        {
            _driver.FindElement(By.Id("Operador2")).SendKeys(operadorB.ToString());
        }

        [When(@"obtengo el resultado de la suma")]
        public void CuandoObtengoElResultadoDeLaSuma()
        {
            new SelectElement(_driver.FindElement(By.Id("Operacion"))).SelectByText("+");
            _driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();
        }

        [Then(@"el resultado es (.*)")]
        public void EntoncesElResultadoEs(int resultado)
        {
            Assert.AreEqual(resultado.ToString(), _driver.FindElement(By.Id("resultdo")).Text);
        }
    }
}
