﻿using System;

namespace Calculator
{
    public interface ICalculator
    {
        int Suma(int a, int b);
        int Resta(int a, int b);
        int Multiplica(int a, int b);
        int Divide(int a, int b);
    }

    public class Calculator : ICalculator
    {
        public int Suma(int a, int b)
        {
            return a + b;
        }

        public int Resta(int a, int b)
        {
            return a - b;
        }

        public int Multiplica(int a, int b)
        {
            return a * b;
        }

        public int Divide(int a, int b)
        {
            if (b==0) throw new ApplicationException();
            return a / b;
        }
    }
}
