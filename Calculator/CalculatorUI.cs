﻿using System;

namespace Calculator
{

    public class CalculatorUi
    {
        private readonly ICalculator _calculadora;

        public CalculatorUi(ICalculator calculadora)
        {
            _calculadora = calculadora;
        }

        public int Operacion(
            string operacion, int op1, int op2)
        {
            switch (operacion)
            {
                case "suma":
                    return _calculadora.Suma(op1, op2);
                case "resta":
                    return _calculadora.Resta(op1, op2);
                case "division":
                    return _calculadora.Divide(op1, op2);
                case "multiplicacion":
                    return _calculadora.Multiplica(op1, op2);
                default:
                    throw new ApplicationException();

            }
        }
    }
}
