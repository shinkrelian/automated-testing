﻿using System.Web.Mvc;
using Calculator;
using MvcCalculator.Models;

namespace MvcCalculator.Controllers
{
    public class CalculatorController : Controller
    {
        public ActionResult Index()
        {
            return View(new CalculatorModel());
        }

        [HttpPost]
        public ActionResult Resultado(CalculatorModel model)
        {
            var calculatorUI = new CalculatorUi(new Calculator.Calculator());
            model.Resultado = calculatorUI.Operacion(model.Operacion, model.Operador1, model.Operador2);
            return View("Index", model);
        }
    }
}
