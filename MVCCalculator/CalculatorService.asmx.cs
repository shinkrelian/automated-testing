﻿using System.Web.Services;
using Calculator;

namespace MvcCalculator
{
    /// <summary>
    /// Summary description for CalculatorService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CalculatorService : System.Web.Services.WebService
    {

        [WebMethod]
        public int Suma(int operador1, int operador2)
        {
            var calculatorUi = new CalculatorUi(new Calculator.Calculator());
            return calculatorUi.Operacion("suma", operador1, operador2);
        }

        [WebMethod]
        public int Resta(int operador1, int operador2)
        {
            var calculatorUi = new CalculatorUi(new Calculator.Calculator());
            return calculatorUi.Operacion("resta", operador1, operador2);
        }
    }
}
