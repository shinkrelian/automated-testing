﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace MvcCalculator.Models
{
    public class CalculatorModel
    {
        public CalculatorModel()
        {
            Operaciones = new List<SelectListItem>
            {
                new SelectListItem
                {
                    Text = "+",
                    Value = "suma"
                },
                new SelectListItem
                {
                    Text = "-",
                    Value = "resta"
                },
                new SelectListItem
                {
                    Text = "x",
                    Value = "multiplicacion"
                },
                new SelectListItem
                {
                    Text = "/",
                    Value = "division"
                }
            };
        }

        public int Operador1 { get; set; }
        public int Operador2 { get; set; }
        public string Operacion { get; set; }
        public int Resultado { get; set; }
        public List<SelectListItem> Operaciones { get; set; }
    }
}