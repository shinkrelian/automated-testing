﻿using System;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace CalculatorTests
{
    [Binding]
    public class LaCalculadoraSumaSteps
    {
        private int _operadorA;
        private int _operadorB;

        [Given(@"que el primer numero es (.*)")]
        public void DadoQueElPrimerNumeroEs(int operadorA)
        {
            _operadorA = operadorA;
        }

        [Given(@"que el segundo numero es (.*)")]
        public void DadoQueElSegundoNumeroEs(int operadorB)
        {
            _operadorB = operadorB;
        }

        [Then(@"el resultado es (.*)")]
        public void EntoncesElResultadoEs(int resultado)
        {
            var calculator = new Calculator.Calculator();
            Assert.AreEqual(resultado, calculator.Suma(_operadorA, _operadorB));
        }
    }
}
