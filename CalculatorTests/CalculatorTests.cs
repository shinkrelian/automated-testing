﻿using System;
using NUnit.Framework;


namespace CalculatorTests
{
    [TestFixture]
    public class CalculatorTests
    {
        private Calculator.Calculator _calculator;

        [TestFixtureSetUp]
        public void SetUpFixture()
        {
            Console.WriteLine("TestFixtureSetUp");
            _calculator = new Calculator.Calculator();
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            Console.WriteLine("TestFixtureTearDown");
        }

        [SetUp]
        public void SetUp()
        {
            Console.WriteLine("SetUp");
        }

        [TearDown]
        public void TearDown()
        {
            Console.WriteLine("TearDown");
        }

        [Test]
        public void ProbarQueUnoMasUnoEsDosTest()
        {
            //Act
            var result = _calculator.Suma(1, 1);


            //Assert
            Assert.AreEqual(2, result, "La suma de uno mas uno no es dos");
        }

        [Test]
        public void ProbarQueUnoMenosUnoEsCeroTest()
        {
            //Act
            var result = _calculator.Resta(1, 1);


            //Assert
            Assert.AreEqual(0, result, "La resta de uno menos uno no es cero");
        }

        [Test]
        public void ProbarQueUnoPorUnoEsUnoTest()
        {
            //Act
            var result = _calculator.Multiplica(1, 1);


            //Assert
            Assert.AreEqual(1, result);
        }

        [Test]
        public void ProbarQue20DivididoPorCuatroEsCincoTest()
        {
            //Act
            var result = _calculator.Divide(20, 4);


            //Assert
            Assert.AreEqual(5, result);
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void ProbarQue20DivididoPorCeroDisparaUnaException()
        {
            //Act
            var result = _calculator.Divide(20, 0);
        }

        [Test]
        public void ProbarQueDosMasTresEsCincoTest()
        {

            //Act
            var result = _calculator.Suma(2, 3);


            //Assert
            Assert.AreEqual(5, result, "La suma de dos mas tres no es cinco");
        }


        [Test]
        public void PruebaSuma(
            [Values(1,2)] int x,
            [Values(1,2)] int y)
        {

            Assert.AreEqual(x+y, _calculator.Suma(x,y));
        }

        [Test]
        public void PruebaResta(
            [Range(1, 8, 2)] int x,
            [Range(1, 2)] int y)
        {

            Assert.AreEqual(x - y, _calculator.Resta(x, y));
        }

        [Test]
        public void PruebaMultiplicacion(
            [Random(1, 5, 2)] int x,
            [Range(2, 3, 3)] int y)
        {

            Assert.AreEqual(x * y, _calculator.Multiplica(x, y));
        }
    }
}
