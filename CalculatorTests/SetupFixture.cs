﻿using System;
using NUnit.Framework;

namespace CalculatorTests
{
    [SetUpFixture]
    public class SetupFixture
    {
        [SetUp]
        public void Setup()
        {
            Console.WriteLine("SetupFixture Setup");
        }

        [TearDown]
        public void TearDown()
        {
            Console.WriteLine("SetupFixture TearDown");
        }
    }
}
