﻿using System;
using Calculator;
using Moq;
using NUnit.Framework;

namespace CalculatorTests
{
    public class CalculatorMock : ICalculator
    {
        public int Suma(int a, int b)
        {
            return 4;
        }

        public int Resta(int a, int b)
        {
            return 4;
        }

        public int Multiplica(int a, int b)
        {
            return 4;
        }

        public int Divide(int a, int b)
        {
            return 4;
        }
    }


    [TestFixture]
    public class CalculatorUiTests
    {
        [Test]
        public void Suma2DosMas2Es4()
        {
            //Arrange
            var calculatorUi = new CalculatorUi(
                new CalculatorMock());

            //Act
            var result = calculatorUi.Operacion("suma", 2, 2);

            //Assert
            Assert.AreEqual(4, result);
        }

        [Test]
        public void SumaSiempreDevuelve5()
        {
            var mockCalculator = new Mock<ICalculator>();

            mockCalculator.Setup(
                p => p.Suma(It.IsAny<int>(), It.IsAny<int>())
                ).Returns(5);

            var calculatorUi = new CalculatorUi(mockCalculator.Object);

            Assert.AreEqual(5,calculatorUi.Operacion("suma", 1, 1));
            Assert.AreEqual(5,calculatorUi.Operacion("suma", 3, 8));
            Assert.AreEqual(5,calculatorUi.Operacion("suma", 9, 5));
        }

        [Test]
        [ExpectedException(typeof(ApplicationException))]
        public void DivisionEntre0LanzaExecepcion()
        {
            var mockCalculator = new Mock<ICalculator>(MockBehavior.Strict);

            mockCalculator.Setup(
                p => p.Divide(It.IsAny<int>(),
                    It.IsIn(new int[] {0}))
                ).Throws(new ApplicationException());

            var calculatorUi = new CalculatorUi(
                new CalculatorMock());

            calculatorUi.Operacion("divide", 4, 0);
        }

        [Test]
        public void Suma4VecesCon5()
        {
            var mockCalculator = new Mock<ICalculator>();

            var calculatorUi = new CalculatorUi(mockCalculator.Object);

            calculatorUi.Operacion("suma", 5, 1);
            calculatorUi.Operacion("suma", 5, 8);
            calculatorUi.Operacion("suma", 5, 2);
            calculatorUi.Operacion("suma", 5, 10);

            mockCalculator.Verify(
                p => p.Suma(It.Is<int>(val => val == 5), It.IsAny<int>()),
                Times.Exactly(4));
        }
    }
}
